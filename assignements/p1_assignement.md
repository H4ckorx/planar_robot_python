# Practical 1

## Requirements

- `ROS2 Iron` installed along with Ubuntu 22.04

- Forked repository of the original project `planar_robot_python`

- Forked repository of the original project `custom_messages`

## Assignment

Create a node `trajectory_sample` complying with the following characteristics:

- Subscribed to a topic named `/time`, using messages of type `Float32`;

- Publisher in a topic named `/desired_state`, using messages of type `CartesianState`;

- The node should have a member one object of type `PositionInterpolation`, taking as parameters

    - `pi = [1.0 2.0]`
    - `pf = [3.0 4.0]`
    - `dt = 2.0`

- When a message is received in topic `/time`, a `CartesianState` is published in `/desired_state` considering the corresponding time and the aforementioned trajectory generation parameters.

In summary, the node `trajectory_sample` should lead to the behavior depicted in *__Animation 1__*.

![complete_gif](../gif/p1.gif)
 ***<p style="text-align: center;">Animation 1: Desired behavior for the trajectory_sample node.</p>***