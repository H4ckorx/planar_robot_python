. src/planar_robot_python/bash_scripts/kill_all_ros.sh
. install/setup.zsh
ros2 launch planar_robot_python planar_robot_python_launch.py   &
rviz2 -d install/planar_robot_python/share/planar_robot_python/urdf/planar_robot.rviz &
ros2 run plotjuggler plotjuggler &

sleep 4
echo "\n=============================================================="
echo "[Enter] ros2 topic pub  /launch std_msgs/msg/Bool {data:true} --once "
echo "------> Load plotjuggler/p5_layout.xml"
read 
ros2 topic pub  /launch std_msgs/msg/Bool "{data: true}" --once 

sleep 2
echo "\n=============================================================="
echo "[Enter] kill all ROS2 processes"
read 
. src/planar_robot_python/bash_scripts/kill_all_ros.sh