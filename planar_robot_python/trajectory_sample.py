import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32
from custom_messages.msg import CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class TrajectorySample(Node):
    def __init__(self):
        super().__init__('trajectory_sample')
        self.publisher_ = self.create_publisher(CartesianState, 'desired_state', 10)
        self.subscription = self.create_subscription(
            Float32,
            'time',
            self.listener_callback,
            10)
        self.subscription
        pi                  = np.array([1.0,2.0])
        pf                  = np.array([3.0,4.0])
        self.pos_interp     = PositionInterpolation(pi,pf,2.0)

    def listener_callback(self, time):
        p                   = self.pos_interp.p(time.data)
        pdot                = self.pos_interp.pdot(time.data)
        desired_state       = CartesianState()
        desired_state.x     = p[0]
        desired_state.y     = p[1]
        desired_state.xdot  = pdot[0]
        desired_state.ydot  = pdot[1]
        self.get_logger().info('Publishing x: "%f"' % desired_state.x)
        self.get_logger().info('Publishing y: "%f"' % desired_state.y)
        self.get_logger().info('Publishing xdot: "%f"' % desired_state.xdot)
        self.get_logger().info('Publishing ydot: "%f"' % desired_state.ydot) 
        self.publisher_.publish(desired_state)  
          
 
def main(args=None):
    rclpy.init(args=args)
    trajectory_sample = TrajectorySample()
    rclpy.spin(trajectory_sample)
    trajectory_sample.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()  