import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
import numpy as np

class Disturbance(Node):
    def __init__(self):
        super().__init__('disturbance')
        self.publisher_ = self.create_publisher(JointState, 'qdot_disturbance', 10)
        self.timer_period = 0.01 
        self.timer = self.create_timer(self.timer_period, self.timer_callback)
        
        # Declare parameters and provide default values
        self.declare_parameter('amplitude', 5.0)
        self.declare_parameter('frequency', 1.0)
        
        self.A = self.get_parameter('amplitude').get_value()
        self.omega = self.get_parameter('frequency').get_value()
        self.t = 0.0
        self.disturbance = np.zeros(2)  

    def timer_callback(self):
        # Get current time
        now = self.get_clock().now()
        qdot_dist = JointState()
        qdot_dist.header.stamp = now.to_msg()
        qdot_dist.velocity = [self.A * np.sin(self.omega * self.t)]  
        
        self.t += self.timer_period
        self.publisher_.publish(qdot_dist)
        
        # Getting parameter values dynamically
        self.A = self.get_parameter('amplitude').get_value()
        self.omega = self.get_parameter('frequency').get_value()

# Ensure ROS2 nodes start and stop correctly
def main(args=None):
    rclpy.init(args=args)
    disturbance = Disturbance()
    rclpy.spin(disturbance)
    disturbance.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
