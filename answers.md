# P1 answer
## Result Comments
Mon code est fondamentalement similaire au code fourni par mon professeur. Cependant, j'ai conservé le code de l'enseignant par souci d'exactitude

# P2 answer
## Result Comments
J'ai obtenu la bonne courbe.

# P3 answer
## Merge Comments
J'ai conservé le code de Merge.
## Result Comments
Mon code signale des erreurs dans plotjuggler !
# P4 answer
## Merge Comments
TP non fait.

# P5 answer
## Merge and Result Comments
Au début, je ne comprenais pas ce que bash essayait de faire, ce qui faisait que mon code ne fonctionnait pas correctement dans plotjuggler, mais après avoir parcouru MERGE, j'ai complètement compris.

# P6 answer
## Result Comments
Je ne pense pas qu'il y ait d'erreurs dans mon code, mais après avoir ouvert plotjuggler, il continue à me rappeler qu'il ne peut pas trouver une ou plusieurs courbes. Je pense que c'est peut-être mon fichier de lancement qui a besoin d'être retravaillé !
